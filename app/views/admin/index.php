<?php require APPROOT . '/views/layout/header.php'; ?>

<?php
require_once APPROOT . '/helpers/Session.php';

Session::start();

// Session::display();
?>

<div class="container">



      <form method="post" action="<?php echo URLROOT; ?>/AdminPageController/addnewTuter">

      <div class="row">

          <div class="col-sm">
  
              <input type="text" name="name" class="form-control form-control-sm" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Enter Tutor Name" required>
          </div>


          <div class="col-sm">
              <input type="email" name="email" class="form-control form-control-sm" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
          </div>


          <div class="col-sm">
              <input type="tel"  name="contactNumber" class="form-control form-control-sm" value=""  pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" placeholder="Enter Contact Number" required>
          </div>


      </div>

    <div class="row mt-4">

        <div class="col-sm">
            <input type="text" name="pass" class="form-control form-control-sm" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Password" required>
        </div>

        <div class="col-sm">
            <input type="text" name="rePass" class="form-control form-control-sm" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Re enter Password" required>
        </div>


    </div>
 

      <div class="col">

       

          <div class="col">
          <div class="d-grid gap-2">
         
                  <button class="btn btn-dark" type="submit">Enter Subject</button>
               

            </div>
          </div>


      </div>


  </form>


  <div class="raw pt-3">
  <div class="col">
  <div class="form-outline">

  <input type="text" id="myInput" class="form-control" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
    </div>
  </div>
</div>





    <table class="table" id="myTable">
      <thead class="thead-dark">
        <tr>
    
              <th scope="col">Tutor Name</th>
              <th scope="col">Email</th>
              <th scope="col">Subject</th>
              <th scope="col">Operation</th>
            </tr>
          </thead>
          <tbody>

          <?php if (isset($data)); ?>
          <?php foreach ($data['tut_details'] as $tut): ?>
            <tr>
         
   
            <td><?= $tut['name']; ?></td>
            <td><?= $tut['email']; ?></td>
            <td><?= $tut['subject']; ?></td>
   
            <td><button type="button" class="btn btn-secondary"><a class="nav-link" href="<?php echo URLROOT; ?>/AdminPageController/showTutor/<?= $tut['tutor_id']; ?>">View</a>
                    </button></td>


            </tr>

          <?php endforeach; ?>


          </tbody>
        </table>
</div>





<script >
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>


<?php require APPROOT . '/views/layout/footer.php'; ?>