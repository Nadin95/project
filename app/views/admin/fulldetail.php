<!-- <h1>Admin page</h1> -->


<?php require APPROOT . '/views/layout/header.php'; ?>

<?php
require_once APPROOT . '/helpers/Session.php';

Session::start();

// Session::set('pig',array(
//   'name'=>'nadin',
//   'age'=>28
// ));

// Session::set('test','demo');
// echo Session::get('name','age');

// Session::display();
?>

<div class="container">

<div class="row">
<div class="col-md-12 bg-light text-right">
<form method="post" action="<?php echo URLROOT; ?>/AdminPageController/approveSubject/<?=$data['id'] ?>">
          
            <button type="submit" class="btn btn-primary">Approved</button>

</form>

<form method="post" action="<?php echo URLROOT; ?>/AdminPageController/deleteSubject/<?=$data['id'] ?>">
            
                
                <button type="submit" class="btn btn-danger ml-2">Delete</button>  
</form>

</div>
</div>



<form method="post" action="<?php echo URLROOT; ?>/AdminPageController/editSubject/<?=$data['id'] ?>">

<div class="row">

    <div class="col-sm">
  
            <input type="text" name="name" class="form-control form-control-sm" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" value="<?=$data['name'] ?>" placeholder="Enter Tutor Name" required>
    </div>


    <div class="col-sm">
    <input type="email" name="email" class="form-control form-control-sm" id="exampleInputEmail1" value="<?=$data['email'] ?>" aria-describedby="emailHelp" placeholder="Enter email">
    </div>


    <div class="col-sm">
            <input type="tel" name="contactNumber" name="contactNumber" class="form-control form-control-sm" value="<?=$data['contact_number'] ?>"  pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" placeholder="Enter Contact Number" required>
    </div>


</div>

<div class="row mt-4">
<div class="col-sm">
  
  <input type="text" name="password" class="form-control form-control-sm" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Password" required>
</div>
<div class="col-sm">
  
  <input type="text" name="repass" class="form-control form-control-sm" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Re Enter Password" required>
</div>

<div class="col-sm">
  
  <input type="text" name="rate" class="form-control form-control-sm" aria-label="Sizing example input" value="<?=$data['rate_hr'] ?>" aria-describedby="inputGroup-sizing-sm" placeholder="Rate/Hr" required>
</div>

</div>
<div class="row mt-4">
    <div class="col">
        
        <select name="subject" class="form-select form-select-sm" aria-label=".form-select-sm example" >
        
            <!-- <option  disable>Select Subject</option> -->
            <!-- <option value="Maths">Maths</option> -->
            <option value="Maths">Maths</option>
            <option value="Science">Science</option>
            <option value="Language">Language</option>

           
        </select>
    </div>

    <div class="col">
    <select name="level" class="form-select form-select-sm" name="level" aria-label=".form-select-sm example">
            <!-- <option selected disable>Select Level</option> -->
            <option value="Beginner">Beginner</option>
            <option value="Intermediate">Intermediate</option>
            <option value="Advanced">Advanced</option>
        </select>
    </div>

 

    <div class="col">

        <button type="submit" class="btn btn-primary">Update</button>

  
         


    </div>
</div>

</form>
</div>



<?php require APPROOT . '/views/layout/footer.php'; ?>

