<?php require APPROOT . '/views/layout/header.php'; ?>

<?php 

require_once APPROOT . '/helpers/Session.php';

Session::start();

// Session::set('pig',array(
//   'name'=>'nadin',
//   'age'=>28
// ));

// Session::set('test','demo');
// echo Session::get('name','age');

// Session::display();



?>

<div class="container">



<form method="post" action="<?php echo URLROOT ;?>/TutorPageController/saveSubject">
<div class="row">
    <div class="col">
        
        <select name="subject" class="form-select form-select-sm" aria-label=".form-select-sm example" >
        
            <option selected disable>Select Subject</option>
            <option value="Maths">Maths</option>
            <option value="Science">Science</option>
            <option value="Language">Language</option>
        </select>
    </div>

    <div class="col">
    <select class="form-select form-select-sm" name="level" aria-label=".form-select-sm example">
            <option selected disable>Select Level</option>
            <option value="Beginner">Beginner</option>
            <option value="Intermediate">Intermediate</option>
            <option value="Advanced">Advanced</option>
        </select>
    </div>

    <div class="col">

        <div class="input-group input-group-sm mb-3">
            <span class="input-group-text" id="inputGroup-sizing-sm">rate Pr hr</span>
            <input type="text" name="rate" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Enter Rate per Hour">
        </div>

        <div class="col">
        <div class="d-grid gap-2">
         
                <button class="btn btn-dark" type="submit">Enter Subject</button>
               

        </div>
        </div>


    </div>
</div>

</form>
</div>

<div class="raw pt-3">
  <div class="col">
  <div class="form-outline">

  <input type="text" id="myInput" class="form-control" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
    </div>
  </div>
</div>





<table class="table" id="myTable">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Subject</th>
      <th scope="col">Level</th>
      <th scope="col">Rate/Hour</th>
      <th scope="col">status</th>
    </tr>
  </thead>
  <tbody>
<?php if(!empty($data)) ?>
  <?php foreach ($data['tutorials'] as $tut) : ?>
    <tr>
    <td><?php echo  $tut['id'] ;?></td>
    <td><?php echo  $tut['subject'] ;?></td>
    <td><?php echo  $tut['sub_level'] ;?></td>
    <td><?php echo  $tut['rate_hr'] ;?></td>
    <td><?php echo  $tut['approved_at'] ==0 ?'Not Approved':'Approved'?></td>
    </tr>

   <?php endforeach ;?>

  </tbody>
</table>

<script >
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<?php require APPROOT . '/views/layout/footer.php'; ?>

