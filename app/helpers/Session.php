<?php

//sessioo class 

class Session 
{
//default session value
    private static $_sessionStarted = false;
//start session
    public static function start()
    {
        if(self::$_sessionStarted == false)
        {
            session_start();
            self::$_sessionStarted=true;
        }
      
    }
//set session  values 
    public static function set($key,$value)
    {
        $_SESSION[$key]=$value;

    }
//get sesson value
    public static function get($key,$secondKey=false)
    {

        if($secondKey == true)
        {
            if(isset($_SESSION[$key][$secondKey]))
            {
                return $_SESSION[$key][$secondKey];
            }

        }else
        {

            if(isset($_SESSION[$key]))
            {
                return $_SESSION[$key];
            }

        }
        return false;

      
    }

//display session value 
//if needed 
    public static function display()
    {
        echo '<pre>';
        print_r($_SESSION);
        echo '</pre>';
    }
//distroy session
    public static function  distroy()
    {
//check either session is started
        if(self::$_sessionStarted == true)
        {

            // Session::set('user_id',$userid);
            // Session::set('name',$userName);

            

            session_unset();
            unset($_SESSION['user_id']);
            unset($_SESSION['name']);
            session_destroy();
        
        }
      

    }
}