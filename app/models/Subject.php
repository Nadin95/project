<?php

class Subject
{
    private $db;
    public function __construct()
    {
        require_once '../app/libraries/Database.php';
        $this->db = new Database;
       
    }
//get subject 
    public function getSubject($id){

       
   

        $this->db->query('SELECT * FROM subject WHERE deleted_at=0 AND tutor_id='.$id);
        $result = $this->db->resultSet();

        return $result;
    }
//add subject
    public function addSubject($data)
    {

        $this->db->query('INSERT INTO subject( tutor_id, name, rate_hr,level) VALUES (:id, :subject, :rate, :level)');
        $this->db->bind(':tutor_id', $data['id']);
        $this->db->bind(':name', $data['subject']);
        $this->db->bind(':rate_hr', $data['rate']);
        $this->db->bind(':level', $data['level']);

            //execute 
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        // INSERT INTO subject( tutor_id, name, rate_hr,level) VALUES (:id, :subject, :rate, :level);

    }
}