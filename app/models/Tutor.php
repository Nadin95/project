<?php

class Tutor
{
    // public $name;

    private $db;
    public function __construct()
    {
        require_once '../app/libraries/Database.php';
        $this->db = new Database;
       
    }
//get tutor data
    public function getTutor(){
        // $this->db->query('SELECT *,
        //                     level.id as levelId,
        //                     level.name as levelname,
        //                     level.created_at as levelCreated
        //                     ');

        $this->db->query('SELECT * FROM tutor
        WHERE deleted_at=NULL');
        $result = $this->db->resultSet();

        return $result;
    }

 
//select user buy mail
    public function findUserByEmail($email){
       
        $sql="SELECT * FROM tutor WHERE email LIKE '$email'";

   


        $row=$this->db->select($sql);

       
        if($row)
        {
            return true;
        }else{
            return false;
        }


    }
//login
    public function login($email, $password){

      
        $this->db->login($email,$password);



    }
//logout
    public function logout()
    {
        $this->db->deleteSession();
    }

    public function getTuterById($id){

        

        $sql="SELECT tutor WHERE id = '$id'";
        $this->db->select($sql);
    
    }
//get all tutor data
    public function getAlltutor(){

        

        $sql="SELECT * FROM tutor WHERE deleted_at = 0";
        $result=$this->db->getDisplayData($sql);
        return $result;
    
    }

//register tutor
    public function registerTutor($data)
    {
        $name=$data['name'];
        $password= $data['password'];
        $mail=$data['email'];
        $contact=$data['contact_number'];

       

        $sql="INSERT INTO tutor (name,password,email,contact_number) VALUES ('$name','$password','$mail','$contact')";


        $this->db->execute($sql);


    }
//show edit details in tutor
    public function editDetails($id)
    {
        $sql="SELECT * FROM tutor INNER JOIN classDetail ON classDetail.tutor_id=$id";

        return $this->db->editDetails($sql);


    }
//update tutor
    public function updateTutor($data)
    {
        

        $id=$data['id'];
        $name=$data['name'];
        $email=$data['email'];
        $password=$data['password'];
        $contact_number=$data['contact_number'];

        $sql  = "UPDATE tutor SET  name = '$name', password = '$password', email = '$email', contact_number = '$contact_number' WHERE id = {$id}";

        $this->db->execute($sql);
    }

//delte tutor 
//hard delete
    public function deleteTutor($id)
    {
        $sql  = "DELETE FROM tutor WHERE id='{$id}'"; 

        $this->db->execute($sql);

    }




}