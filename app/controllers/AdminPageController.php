<?php


include_once('../app/config/config.php');
require_once APPROOT . '/helpers/Session.php';

class AdminPageController extends Controller
{

    public $id;
    
  

    public function __construct()
    {       
       
   
   
        $this->tutorialModel=$this->model('Tutorial');
        $this->tutor = $this->model('Tutor');



    }

    //view admin page 
    public function index()
    {

        Session::start();
        // $user_id=$_SESSION['user_id'];

        $tut_details=$this->tutorialModel->getTutorialwithTutor();
     
        $data=[
          
               'tut_details'=>$tut_details
            ];

        

        $this->view('admin/index',$data);

    }

//show individual tutor details after click view
    public function showTutor($id)
    {
      

    
        $tutor_details=$this->tutor->editDetails($id);



        return $this->view('admin/fulldetail',$tutor_details);

     
    }
//edit subjects
    public function editSubject($id)
    {
  

      if($_SERVER['REQUEST_METHOD'] == 'POST')
      {

        
    
          $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING); 

        //   echo 'hi';
        
          $data = [
              'id'=>$id,
            'name' => trim($_POST['name']),
            'email' => trim($_POST['email']),
            'password' => md5(trim($_POST['password'])),
            'contact_number'=>trim($_POST['contactNumber']),

           
           
        ];

        $data1=[
            'id'=>$id,
           'rate' => trim($_POST['rate']),
           'subject' => trim($_POST['subject']),
           'level' => trim($_POST['level'])
        ];

//edit tutor details
        $this->tutor->updateTutor($data);
//edit subject details
        $this->tutorialModel->updateTutorial($data1);


  
   
  
  
      }
  
    }
//delete subjects (Hard Delete)
   public function deleteSubject($id)
   {

    $this->tutor->deleteTutor($id);

    $this->tutorialModel->deleteTutorial($id);


   }
//add new tutor
   public function addnewTuter()
   {
    if ($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        // process form

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING); 
         $data = [
             'name' => trim($_POST['name']),
             'email' => trim($_POST['email']),
             'password' => md5(trim($_POST['pass'])),
             'contact_number'=>trim($_POST['contactNumber'])
            
         ];


         //make sure error are empty
         
      
             if($this->tutor->registerTutor($data)){
                 header("Location: tutor/login");
             }
             // redirect('tutor/login');
            

         
     }

   }
//approve subjecst
  public function approveSubject($id)
  {

    $this->tutorialModel->approveSubject($id);

    

  }





}
