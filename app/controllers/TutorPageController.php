<?php


include_once('../app/config/config.php');
require_once APPROOT . '/helpers/Session.php';
//controller for view tutor page 
class TutorPageController extends Controller
{

    public $id;
    
  

    public function __construct()
    {       
       
   
   
        $this->tutorialModel=$this->model('Tutorial');
        $this->tutor = $this->model('Tutor');



    }
//show tutor page
    public function index()
    {
        

        Session::start();
        $user_id=$_SESSION['user_id'];

        $tutorials=$this->tutorialModel->getTutorial($user_id);

        // $tutor=$this->tutor->getTuterById();

        $data=[
            //'Subjects'=>$Subjects,
               'tutorials'=>$tutorials
            ];

        return $this->view('tutor/index',$data);

    }

//save subjects from tutor

    public function saveSubject()
    {
 
    
     if($_SERVER['REQUEST_METHOD'] == 'POST')
         {
            Session::start();
       
 
             $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING); 
            
             $data = [
                 'tutor_id'=>trim($_SESSION['user_id']),
                 'tutor_rate' => trim($_POST['rate']),
                 't_subject' => trim($_POST['subject']),
                 't_level' => trim($_POST['level'])
              

             ];


 
 
             if($this->tutorialModel->addTutorial($data)){
                 header("Location: TutorPageController/index");
             }
 
 
         }
    }





}