<?php
  /*
   * PDO Database Class
   * Connect to database
   * Create prepared statements
   * Bind values
   * Return rows and results
   */

  include_once('../app/config/config.php');
  require_once APPROOT . '/helpers/Session.php';
  // include('../app/controllers/TutorController.php');;

  

  class Database {

    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;

    private $dbh;
    private $stmt;
    private $error;
    private $conn;
// create DB conection
    public function __construct(){
      
        $this->conn = new mysqli($this->host,$this->user, $this->pass,$this->dbname);

      // Check connection
          if ($this->conn->connect_error) {
                die("Connection failed: " . $this->conn->connect_error);
      }

        
    }


       // Prepare statement with query
       public function query($sql){
        $this->stmt = $this->conn->prepare($sql);

        if($this->stmt){
          return true;
          
        }else{
          return false;
        }
      }

   


    public function execute($sql)
    {



      if($this->conn->query($sql) === TRUE) {
        // echo "<p>New Record Successfully Created</p>";

        return true;
      
      } else {
        echo "Error in :-" . $sql . ' ' . $this->conn->connect_error;
      }
    
      $this->conn->close();


    }

    //select data
    function select($sql)
    {
     
      // $results = mysqli_query(, $sql);
      $query = mysqli_query($this->conn,$sql)or die(mysqli_error());
      // $check = mysqli_fetch_array($result);

      $rows = mysqli_num_rows($sql);
      $fetch = mysqli_fetch_array($query);

     
      if($rows > 0){
        // echo 'success';
        return true;
        }else{
        // echo 'failure';
        echo "Error in :-" . $sql . ' ' . $this->conn->connect_error;
        }
      
    }

//login
    function login($email,$password)
    {
        $email = mysqli_real_escape_string($this->conn,$email);
        $pass = mysqli_real_escape_string($this->conn,$password);

  

     

        $hashed_password = md5($pass);
        $sql = "SELECT * FROM tutor WHERE email='$email' AND password='$hashed_password'";

      

      
        $query = mysqli_query($this->conn, $sql);

        // var_dump ($query);
        $res=mysqli_num_rows($query);
        
        //If result match $username and $password Table row must be 1 row
        if($res == 1)
        {

          $row=mysqli_fetch_assoc($query);

          $userid=$row['id'];
          $userName=$row['name'];



          // echo ($userid);
         
          session::start();
          Session::set('user_id',$userid);
          Session::set('name',$userName);
          // Session::display();
        
          
        header("Location:".URLROOT."/TutorPageController/index");

        // $this->tutor_detail->index();



        }
        else
        {
          // echo '<script>alert("Invalide Username or Password")</script>';
        // header("Location:".URLROOT."/TutorPageController/loginView=?");

        header("Location:".URLROOT."/TutorController/loginView?Wrong Username or parrword= Please try again");
        }


     
      
    }
//delete session data
    public function deleteSession()
    {
  
    
      Session::distroy();
        

    }

//get data as row for for each in views
    public function getDisplayData($sql)
    {

      $result = $this->conn->query($sql);
      // ->fetch_all(MYSQLI_ASSOC);

      if($result->num_rows > 0){
        return $result; 
    }else{
        return false;
    }
    }
//edit data
    public function editDetails($sql)
    {
      $result = $this->conn->query($sql);

      return $result->fetch_assoc();
    }


    

  


   

  }