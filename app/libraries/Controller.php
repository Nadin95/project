<?php

class Controller
{
//call model folder
    public function model($model)
    {
        require_once '../app/models/' .$model. '.php';

        return new $model();
    }

//call view dir
    public function view($view, $data= [])
    {

        require_once '../app/views/' .$view. '.php';

    }
  
}