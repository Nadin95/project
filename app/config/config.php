<?php

//configurations   DB variables 

  // DB Params
  define('DB_HOST', 'localhost');
  define('DB_USER', 'root');
  define('DB_PASS', '');
  define('DB_NAME', 'digitaltutor');

  // App Root
  define('APPROOT', dirname(dirname(__FILE__)));
  // URL Root
  define('URLROOT', 'http://localhost/Projects/coursework/public');
  // Site Name
  define('SITENAME', 'Digital Tutors');
  // App Version
  // define('APPVERSION', '1.0.0');
 